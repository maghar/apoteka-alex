import React from 'react';

const DropDown = ({ input, list, placeholder }) => {

    const values = list.map((value, index) => {
        return (
            <option value={value} key={index}>{value}</option>
        )
    });

    return (
        <select {...input}
            className="form-control">
            <option value=''>{placeholder}</option>
            {values}
        </select>
    );
}

export default DropDown;