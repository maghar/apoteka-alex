import React from "react";
import { Field } from 'redux-form';
import propTypes from 'prop-types';

const Input = (props) => {
    const { label, width } = props;
    const elementWidth = width ? `col-md-${width}` : `col-md-12`;
    return (
        <div className={elementWidth}>
            <div className="form-group">
                <label>
                    {label}
                </label>
                <Field
                    className="form-control"
                    {...props}
                />
            </div>
        </div>
    )
}

Input.propTypes = {
    label: propTypes.string,
    width: propTypes.string
}

export default Input;