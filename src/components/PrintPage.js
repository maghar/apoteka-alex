import React from 'react';
import { connect } from 'react-redux';
import hck from '../images/hck.png';
import sw from '../images/sw.png';
import aa from '../images/aa.png';
import Table from './Table';
import '../styles/PrintPage.css';

const PrintPage = ({ className, form }) => {
    if (form && form.file) {
        const dailyDose = form.dailyDose ? Number(form.dailyDose.replace(',', '.')) : null;
        const coff = 1.717;
        return (
            <div className={className} >
                <div className="print-container">
                    <div className="meta">
                        <div className="logo-container">
                            <img className="img-fluid" width="195" src={hck} alt="hck" />
                            <img className="img-fluid absolute-logo" width="65" src={sw} alt="sw" />
                        </div>
                        <div className="separate"></div>
                        <p className="sub-title">personal micro-nutrients mixed by</p>
                        <img className="main-logo" src={aa} alt="sw" />
                        <div className="personal-data">
                            <p>for</p>
                            <div className="name">
                                <p>{form.firstName} {form.surName}</p>
                            </div>
                            <div className="date">
                                <p>{form.startMonth} - {form.finishMonth} {form.year}</p>
                            </div>
                        </div>
                        <div className="doses">
                            <p>Your daily dose - {(dailyDose * coff).toFixed(1)} ml. It is equal to {form.dailyDose} g.</p>
                            <small>Mix half of the daily amount with 100-250 ml of your preferred beverage or a paste-like food yogurt, cottage cheese, etc. and consume right away in the mornings and in the evenings.</small>
                        </div>
                    </div>
                    <div className="compound">
                        <Table table={form.file.result} />
                    </div>
                </div>
            </div>
        )
    } else {
        return (
            <div className={className}>
                <div>Empty form</div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    if (state.form.recipe && state.form.recipe.values) {
        return {
            form: state.form.recipe.values
        }
    } else {
        return {
            form: null
        }
    }
}

export default connect(mapStateToProps, null)(PrintPage);