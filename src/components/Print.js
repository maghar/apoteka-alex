import React, { Component, Fragment } from 'react';
import ReactToPrint from 'react-to-print';
import PrintPage from './PrintPage';
import propTypes from 'prop-types';

class Print extends Component {
    render() {
        return (
            <Fragment>
                <ReactToPrint
                    trigger={() => this.props.print}
                    content={() => this.printTable}
                />
                <PrintPage className='hidden-print'
                    ref={el => (this.printTable = el)} />
            </Fragment>
        )
    }
}

Print.propTypes = {
    print: propTypes.object
}

export default Print;