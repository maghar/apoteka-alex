import React from 'react';
import { separateArr, tableSize, replacer } from '../utils/table';
import uuidv1 from 'uuid/v1';

const Table = ({ table }) => {
    const tableEdit = table.slice(1);
    let maxElements = 26;
    const columns = 3;
    const small = tableSize(tableEdit, maxElements, columns);
    if (small) {
        maxElements = 31;
    }
    const renderTable = separateArr(tableEdit, maxElements, columns).map((item, index) => {
        return (
            <div key={index} className="column">
                {
                    item.map(subItem => {
                        const smallItem = small ? "small-row row-table" : "row-table";
                        const category = !subItem[1] ? "category" : "";
                        const formatName = subItem[0].replace(/alpha|beta|gamma/g, replacer);
                        return (
                            <div className={`${smallItem} ${category}`} key={uuidv1()}>
                                <p>{formatName}</p>
                                <p>{subItem[1]} {subItem[2]}</p>
                            </div>
                        )
                    })
                }
            </div>
        )
    });

    return (
        <div className="table">
            {renderTable}
        </div>
    )
}

export default Table;