import React, { Component } from 'react';
import { connect } from 'react-redux';

import Dropzone from 'react-dropzone';
import Papa from 'papaparse';
import csvIcon from '../images/csv-icon.jpg';
import propTypes from 'prop-types';

const baseStyle = {
    width: 120,
    height: 120,
    borderWidth: 2,
    borderColor: '#666',
    borderStyle: 'dashed',
    borderRadius: 10,
    position: 'relative',
    cursor: 'pointer'
};

const activeStyle = {
    backgroundImage: `url(${csvIcon})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center'
}

class DropZone extends Component {
    onDrop = (file) => {
        if (file[0]) {
            const reader = new FileReader();
            const { input: { onChange } } = this.props;
            reader.onload = () => {
                const parseResult = Papa.parse(reader.result);
                const fileObject = {
                    info: {
                        name: file[0].name,
                        size: file[0].size
                    },
                    result: parseResult.data
                }
                onChange(fileObject)
            };
            reader.readAsBinaryString(file[0]);
        }
    }

    render() {
        const { name, file } = this.props;
        let activeField = null;
        let fileName = '';
        if (file) {
            activeField = activeStyle;
            fileName = <p className="form-text text-muted">{file.info.name} - Size: {file.info.size} bytes</p>
        } else {
            fileName = <p className="form-text text-muted text-danger">We support only .csv format</p>
        }
        const styles = { ...baseStyle, ...activeField };
        return (
            <div>
                <Dropzone onDrop={this.onDrop}
                    name={name}
                    accept=".csv" >
                    {({ getRootProps, getInputProps }) => {
                        return (
                            <div {...getRootProps()}
                                style={{ ...styles }}
                            >
                                <input {...getInputProps()} />
                                {!file ? <p className="drop-file-label">Drag file here</p> : null}
                            </div>
                        )
                    }}
                </Dropzone>
                <div className="file-name">
                    {fileName}
                </div>
            </div>
        )
    }
}

DropZone.propTypes = {
    file: propTypes.object,
    name: propTypes.string
}

const mapStateToProps = state => {
    if (state.form.recipe &&
        state.form.recipe.values &&
        state.form.recipe.values.file) {
        return {
            file: state.form.recipe.values.file
        }
    } else {
        return {
            file: null
        }
    }
}

export default connect(mapStateToProps, null)(DropZone);