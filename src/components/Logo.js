import React from 'react';
import logo from '../images/aa.png';

const Logo = () => {
    return (
        <div className="logo">
            <img className="img-thumbnail" src={logo} alt="Logo" />
        </div>
    )
}

export default Logo;