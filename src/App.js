import React, { Component } from 'react';
import Recipe from './containers/Recipe';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Recipe />
      </div>
    );
  }
}

export default App;
