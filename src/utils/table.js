export const transformTable = (table, col) => {
    const divideIndex = Math.ceil(table.length / col);
    let divideTable = [];
    table.forEach((item, index) => {
        if (index % divideIndex === 0) {
            divideTable = [...divideTable, table.slice(index, index + divideIndex)];
        }
    });
    return divideTable;
}

export const separateArr = (table, maxElements, col) => {
    let divideTable = [];
    const separatorTable = Math.ceil(table.length / col);
    let maxRows = maxElements;
    if (separatorTable > maxRows) {
        maxRows = separatorTable;
    }
    table.forEach((item, index) => {
        if (index % maxRows === 0) {
            divideTable = [...divideTable, table.slice(index, index + maxRows)];
        }
    });
    return divideTable;
}

export const tableSize = (table, maxElements, col) => {
    return table.length > maxElements * col ? true : false;
}

export const replacer = (match) => {
    switch (match) {
        case 'alpha':
            return 'α'
        case 'beta':
            return 'β'
        case 'gamma':
            return 'γ'
        default:
            return match;
    }
}