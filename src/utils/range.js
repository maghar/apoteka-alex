export const range = (begin, step, range) => {
    let rangeArr = [];
    let result = begin;
    for (let i = 0; i < range; i++) {
        rangeArr = [...rangeArr, Math.ceil(result * 10) / 10];
        result += step;
    }
    return rangeArr;
}

export const rangeMonth = () => {
    const monthList = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];
    return monthList;
}