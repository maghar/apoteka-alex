import React, { Component } from 'react';
import RecipeForm from './RecipeForm';
import Logo from '../components/Logo';

class Recipe extends Component {
    render() {
        return (
            <div className="mt-4">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <Logo />
                        </div>
                        <div className="col-md-12">
                            <RecipeForm />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
};

export default Recipe;