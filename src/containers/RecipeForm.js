import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import DropZone from '../components/DropZone';
import DropDown from '../components/DropDown';
import Print from '../components/Print';
import Input from '../components/Input';
import { connect } from 'react-redux';
import { rangeMonth, range } from '../utils/range';
import PropTypes from 'prop-types';

const required = value => value ? undefined : 'Required'

class RecipeForm extends Component {
    state = {
        formValid: false,
        showError: false
    }

    handleClean = () => {
        const { reset } = this.props;
        reset();
    }

    handleClick = () => {
        this.setState({
            showError: true
        })
    }

    componentDidUpdate(prevProps) {
        if (this.props.form !== prevProps.form) {
            if (this.props.form && !this.props.form.syncErrors) {
                this.setState({
                    formValid: true,
                    showError: false
                })
            } else {
                this.setState({
                    formValid: false
                })
            }
        }
    }

    render() {
        const button = <button className="btn btn-primary" type="submit" onClick={this.handleClick}>Print</button>;
        const printButton = <button className="btn btn-primary" type="submit">Print</button>;
        const errorMessage = <p className="text-danger">All fields are required</p>;
        const month = rangeMonth();
        const year = range((new Date()).getFullYear(), 1, 5);
        return (
            <form id="recipe"
                className="mt-3"
                onSubmit={this.props.handleSubmit}>
                <div className="row">
                    <Input
                        name="firstName"
                        component="input"
                        type="text"
                        placeholder="First Name"
                        label="First Name"
                        width="6"
                        validate={required}
                    />
                    <Input
                        name="surName"
                        component="input"
                        type="text"
                        placeholder="Surname"
                        label="Surname"
                        width="6"
                        validate={required}
                    />

                    <Input
                        name="startMonth"
                        component={DropDown}
                        placeholder="Select month"
                        label="Start Month"
                        width="6"
                        validate={required}
                        list={month}
                    />
                    <Input
                        name="finishMonth"
                        component={DropDown}
                        placeholder="Select month"
                        label="Finish Month"
                        width="6"
                        validate={required}
                        list={month}
                    />
                    <Input
                        name="year"
                        component={DropDown}
                        label="Year"
                        width="12"
                        placeholder="Select year"
                        validate={required}
                        list={year}
                    />
                    <Input
                        name="dailyDose"
                        component="input"
                        type="text"
                        placeholder="Daily dose"
                        label="Daily dose, g."
                        width="6"
                        validate={required}
                    />
                </div>
                <div className="row">
                    <Input
                        label="File"
                        name="file"
                        component={DropZone}
                        width="6"
                        validate={required}
                    />

                    <div className="col-md-12">
                        {this.state.showError ? errorMessage : null}
                        <div className="print-wrapper">
                            {this.state.formValid ? <Print print={printButton} /> : button}
                            <button className="btn btn-danger"
                                type="button"
                                onClick={this.handleClean}>Clear</button>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}

RecipeForm.propTypes = {
    handleSubmit: PropTypes.func,
    reset: PropTypes.func,
    form: PropTypes.object,

}

const mapStateToProps = state => {
    return {
        form: state.form.recipe
    }
};

RecipeForm = connect(mapStateToProps, null)(RecipeForm);

export default reduxForm({
    form: 'recipe',
    onSubmit: () => {}
})(RecipeForm);
