import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from './reducers';
import './styles/index.css';

const store = createStore(reducers, composeWithDevTools());

ReactDOM.render(<Provider store={store}>
  <App />
</Provider>, document.getElementById('root'));